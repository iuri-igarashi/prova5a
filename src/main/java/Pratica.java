
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Scanner sc = new Scanner(System.in);
        String path;
        System.out.println("digite o caminho:");
        path = sc.next();
        List lista;
        
        ProcessaLancamentos pl = new ProcessaLancamentos(path);
        lista = pl.getLancamentos();
        
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        for (Lancamento j: lancamentos) {
            System.out.println(j.toString());
        }
    }
 
}