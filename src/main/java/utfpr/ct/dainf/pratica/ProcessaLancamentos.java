package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Linguagem Java
 *
 * @author
 */
public class ProcessaLancamentos {

    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException, IOException {
        //throw new UnsupportedOperationException("Não implementado");
        if (arquivo == null) {
            System.out.print("Arquivo nao encontrado");
        } else {
            reader = new BufferedReader(new FileReader(arquivo.getPath()));
        }
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException, IOException {
        //throw new UnsupportedOperationException("Não implementado");
        if (path == null) {
            System.out.print("Arquivo nao encontrado");
        } else {
            reader = new BufferedReader(new FileReader(path));
        }
    }

    private String getNextLine() throws IOException {
        //throw new UnsupportedOperationException("Não implementado");
        String linha;
        linha = reader.readLine();
        return linha;
    }

    private Lancamento processaLinha(String linha) {
        //throw new UnsupportedOperationException("Não implementado");
        Integer conta;
        Date data;
        String descricao;
        Double valor;
        Lancamento lancamento;
        
        conta = Integer.parseInt(linha.substring(0,6));
        data = new Date(Integer.parseInt(linha.substring(6,14)));
        descricao = linha.substring(14,74);
        valor = Double.parseDouble(linha.substring(74,86));
        
        lancamento = new Lancamento(conta, data, descricao, valor);
        
        return lancamento;
    }

    private Lancamento getNextLancamento() throws IOException {
        //throw new UnsupportedOperationException("Não implementado");
        String linha;
        linha = reader.readLine();
        processaLinha(linha);
        Lancamento lancamento;
        
        if(linha != null)
            lancamento = processaLinha(linha);
        else
            lancamento = null;
        
        return lancamento;
    }

    public List<Lancamento> getLancamentos() throws IOException {
        //throw new UnsupportedOperationException("Não implementado");
        List<Lancamento> lista = new ArrayList<>();
        Long cont = 0L;
        Lancamento lancamento;
        String linha;
        
        while((lancamento = getNextLancamento()) != null)
            lista.add(lancamento);
        
        return lista;
    }

}
